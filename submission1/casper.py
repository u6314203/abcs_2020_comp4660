"""
Defining casper NN model.
"""
import pandas as pd
import numpy as np
import torch
import torch.nn as nn

class Casper(nn.Module):
    def __init__(self, input_dim=10, output_dim=2, delta=100, epsilon=0.01, lr_initial=0.2):
        super(Casper, self).__init__()
        self.input_dim = input_dim
        self.output_dim = output_dim
        self.delta = delta
        self.epsilon = epsilon
        self.activation = nn.Sigmoid()

        # weights connecting input directly to output
        self.input_output = nn.Linear(input_dim, output_dim)
        # each element at i: for hidden neuron i, its weight (in_from_input, out_to_output)
        self.hidden_io = []
        # each element at i: [weights from previous neurons to i]
        self.hidden_hidden = [None]
        # dict recording gradient of last step
        self.weights_prev_gd = dict()
        # dict mataining learning rate for certain weights(vector/matrix)
        self.weights_lr = dict()

        # init lr for input_output weights
        for p in self.input_output.parameters():
            lrs = torch.zeros_like(p.data)
            lrs.add_(lr_initial)
            self.weights_lr[p] = lrs

    def forward(self, x):
        """IMPORTANT: add activation between input_output"""
        if not self.hidden_io:
            out = self.input_output(x)
        else:
            if len(self.hidden_io) == 1:
                h_in, h_out = self.hidden_io[0]
                out = self.input_output(x) + h_out(self.activation(h_in(x)))
            else:
                out = self.input_output(x)
                hidden_acts = [] # all hidden activation values before
                for i, hidden_neu in enumerate(self.hidden_io):
                    h_in, h_out = self.hidden_io[i]
                    act = None # activation for hidden i

                    if i == 0: # first hiddern
                        act = self.activation(h_in(x))
                    else:
                        act = h_in(x) # from input
                        for j, h_act_j in enumerate(hidden_acts): # from all previous hidden
                            h_h = self.hidden_hidden[i][j] # from hidden j to hidden i
                            act += h_h(h_act_j)
                        act = self.activation(act)

                    hidden_acts.append(act)
                    out += h_out(act)

        return out

    def add_neuron(self):
        # print("Add a new hidden neuron!")
        # add a neuron when criterion met
        self.hidden_io.append((nn.Linear(self.input_dim, 1), nn.Linear(1, self.output_dim)))
        if len(self.hidden_io) > 1:
            h_h_connections = []
            for n in range(len(self.hidden_hidden)):
                h_h_connections.append(nn.Linear(1,1))
            self.hidden_hidden.append(h_h_connections)
        # update lrs required: done by optimizer

    def criterion_add_neuron(self, eva_losses, epoches_add_neuron):
        """ eva_losses: all evaluation set loss """
        if not epoches_add_neuron:
            epoch_last_add = 0
        else:
            epoch_last_add = epoches_add_neuron[-1]
        if epoch_last_add ==0: # first phase
            if len(eva_losses) == 1:
                return False
            elif 1 < len(eva_losses) <= self.delta:
                return eva_losses[0] - eva_losses[-1] < self.epsilon
            else:
                return eva_losses[-self.delta] - eva_losses[-1] < self.epsilon
        if len(eva_losses) - epoch_last_add <= self.delta:
            return False
        if (len(eva_losses) - epoch_last_add) % self.delta != 0:
            return False
        else:
            return eva_losses[-self.delta] - eva_losses[-1] < self.epsilon

    def criterion_halt(self, eva_losses, epoches_add_neuron, training_accs=None):
        k = 15 # maximum neuros
        eta = 3 # number allowing added neuron no effect
        if len(epoches_add_neuron) >= k:
            return True
        elif training_accs[-1] >= 0.7:
            # early stopping when 70% train accuracy stabaly reached
            # note in generall this is bad idea
            if len(training_accs)<3:
                return False
            else:
                return training_accs[-2] >= 0.7 and training_accs[-3] >= 0.7
        # elif len(epoches_add_neuron) >= eta:
        #     return eva_losses[epoches_add_neuron[-eta]] - eva_losses[-1] < self.epsilon
        else:
            return False

"""
Progressive RPROP training algorithm, here in short RPROP, one form of optimize;
its coupled regularization is also defined: 3-norm with simulated annealing.
"""
import pandas as pd
import numpy as np
import torch
import torch.nn as nn

class RPROP():
    def __init__(self, lr_region=(0.25, 0.005, 0.001), lr_update=(1.2, 0.5), lr_limit= (1e-6, 50)):
        """
        parameters
            param: network learnable parameteres
            lr_region: initial step size of hidden weights determined by region
            lr_update: coefficient for increase or decrease step size
            decay: simulated annealing decay
        """
        # No check on the validity of configuration
        self.lr_l1, self.lr_l2, self.lr_l3 = lr_region
        self.step_inc, self.step_dec = lr_update
        self.Hepoch = 0 # epoch from last add hidden neuron
        self.lr_limit = lr_limit # min and max leraning rate, currently set to fixed

    def step(self, net):
        """since neurons are chaging in casper, the weights to learn are passed in every step
        """
        self.Hepoch +=1
        params_lrs = net.weights_lr # learnable parameters with associated learning rates,
        params_prev_gd = net.weights_prev_gd # learnable parameters with gradients from last step

        # limit lr to a certain range
        def limit(x):
            if x > self.lr_limit[1]:
                return self.lr_limit[1]
            if x < self.lr_limit[0]:
                return self.lr_limit[0]
            else:
                return x
        limit_vec = np.vectorize(limit)

        for p, lr in params_lrs.items(): # each parameter vector
            if p.grad is None:
                continue
            grad = p.grad.data
            if p not in params_prev_gd: # first step for this parameter
                p.data.sub_(lr*torch.sign(grad))
            else:
                prev_grad = params_prev_gd[p]
                indicator = ((prev_grad * grad) > 0).float()
                lr = lr*indicator*self.step_inc + lr*(1-indicator)*self.step_dec
                lr = torch.Tensor(limit_vec(lr.numpy()))
                params_lrs[p] = lr # update lr dict
                p.data.sub_(lr*torch.sign(grad))
            params_prev_gd[p] = grad # update previous gradient

    def update_lrs(self, net):
        """ add a new neuron: update learning rates of net """
        self.Hepoch = 0
        # initialise learning rates of newly added connection
        input_h, h_output = net.hidden_io[-1]
        for p in input_h.parameters():
            lrs_init = torch.zeros_like(p.data)
            lrs_init.add_(self.lr_l1)
            net.weights_lr[p] = lrs_init
        for p in h_output.parameters():
            lrs_init = torch.zeros_like(p.data)
            lrs_init.add_(self.lr_l2)
            net.weights_lr[p] = lrs_init
        if net.hidden_hidden[-1] is not None:
            for h_h in net.hidden_hidden[-1]:
                for p in h_h.parameters():
                    lrs_init = torch.zeros_like(p.data)
                    lrs_init.add_(self.lr_l1)
                    net.weights_lr[p] = lrs_init
        # reset lrs of other weights
        if net.hidden_hidden[-1] is None: # just added the first hidden
            # update lrs of input_output
            for p in net.input_output.parameters():
                lrs = torch.zeros_like(p.data)
                lrs.add_(self.lr_l3)
                net.weights_lr[p] = lrs
        else:
            # update lrs of previous hidden
            input_h, h_output = net.hidden_io[-2]
            for p in input_h.parameters():
                lrs_init = torch.zeros_like(p.data)
                lrs_init.add_(self.lr_l3)
                net.weights_lr[p] = lrs_init
            for p in h_output.parameters():
                lrs_init = torch.zeros_like(p.data)
                lrs_init.add_(self.lr_l3)
                net.weights_lr[p] = lrs_init

def regularized_loss(loss_raw, params, Hepoch, T=0.05):
    """ 3-norm regularization of simulated annealing,
        used in pair with RPROP
        params: all parameters in network
    """
    # fix some hyperparameters
    lam = 0.01
    T = T
    S = 2**(-Hepoch*T)

    params_vector = torch.cat([x.view(-1) for x in params])
    regularization = lam / 3 * S * torch.norm(params_vector, 3)
    loss_reg = loss_raw + regularization
    return loss_reg

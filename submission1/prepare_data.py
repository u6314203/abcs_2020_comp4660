"""
1. preprocessing of raw dataset
2. train test split for cross validation
"""

import pandas as pd
import numpy as np
from sklearn.preprocessing import MinMaxScaler
import os

dataset_path = "./dataset/thermal-RGB-components.xlsx"
dataset = pd.read_excel(dataset_path)

"""Preprocessing"""
# 0,1 label encoding
dataset['label'] = dataset.apply(lambda x: 1 if x["Labels"] == "Stressful" else 0, axis=1)
preproc_base = dataset[dataset.columns[2:]]
preproc_base.to_excel("./dataset/preproc_base_thermal.xlsx", index=False) # write to same folder of raw dataset

# [-1,1] normalisation over all entries
features = dataset.columns[2:-1]
X_features = dataset[features]
scalar = MinMaxScaler(feature_range=(-1,1))
X_norm = scalar.fit_transform(X_features)
Y = dataset[["label"]]
cols = ["RGB_pca1", "RGB_pca2","RGB_pca3","RGB_pca4","RGB_pca5",\
        "Thermal_pca1", "Thermal_pca2","Thermal_pca3","Thermal_pca4","Thermal_pca5", "label"]
preproc_norm = pd.DataFrame(np.concatenate((X_norm, Y), axis=1), columns=cols)
preproc_norm.to_excel("./dataset/preproc_norm_thermal.xlsx", index=False) # write to same folder of raw dataset


"""Train test split: 10-fold cross validation"""
N, D = preproc_norm.shape
D = D -1
np.random.seed(0)
indicies = np.array(range(N))
np.random.shuffle(indicies)
fold_size = int(N/10) #62
folds = []
for i in range(10):
    # create 10 fold
    fold_i = indicies[[x for x in range(i*fold_size, (i+1)*fold_size)]]
    folds.append(fold_i)

for i in range(10):
    test_partition = [i]
    eva_partition = [(i+1)%10, (i+2)%10]
    train_partition = [(i+n)%10 for n in range(3,10)]

    test_ind, eva_ind, train_ind = [], [], []
    for ele in test_partition:
        test_ind.extend(folds[ele])
    for ele in eva_partition:
        eva_ind.extend(folds[ele])
    for ele in train_partition:
        train_ind.extend(folds[ele])

    test_set, eva_set, train_set = preproc_norm.iloc[test_ind], preproc_norm.iloc[eva_ind], preproc_norm.iloc[train_ind]
    os.mkdir(f"./dataset/fold{i}")
    test_set.to_excel(f"./dataset/fold{i}/test_set.xlsx", index=False)
    eva_set.to_excel(f"./dataset/fold{i}/eva_set.xlsx", index=False)
    train_set.to_excel(f"./dataset/fold{i}/train_set.xlsx", index=False)

print("Done generating 10-fold dataset in ./dataset/")

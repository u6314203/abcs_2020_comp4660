# README

Report: [Convergency ability and speed of Constructive Cascade Network](./u6314203.pdf)

## Dataset

All datasets are placed in the folder *dataset*:

+ thermal-RGB-components: original dataset
+ preproc_base_thermal: recode and discard certain columns
+ preproc_norm_thermal: normalize all feature columns

## Training, evaluating, testing

### step1: generate 10-fold

Run 

```python
python prepare_data.py
```

10 folds data shall be generated in *dataset* for 10-fold cross validation

### step2: train and test casper

```python
usage: train_test.py [-h] [--delta DELTA] [--lr_init LR_INIT]
                     [--lr_region LR_REGION] [--T T]
                     log_repo_name

Training, evaluating, testing

positional arguments:
  log_repo_name         the repo used to place logs(text & image)

optional arguments:
  -h, --help            show this help message and exit
  --delta DELTA         period to evaluate add_neuron criterion
  --lr_init LR_INIT     initial learning rate in first training phase
  --lr_region LR_REGION
                        initial learning rates of three different regions,
                        format: l1,l2,l3
  --T T                 temperature constant in simulated annealing
```

The only required parameter is *log_repo_name*, which shall be created under *logs* and will be used to place all information of this training and testing process.

## Contact

For any issue, please contact email: xinqi.zhu@anu.edu.au
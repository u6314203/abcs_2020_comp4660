"""
Assumed data is prepared(by calling prepare_data.py) and located in "./dataset/",
train, evaluate, test the casper model in a 10-fold cross validation manner.
"""
import pandas as pd
import numpy as np
import torch
import torch.nn as nn
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('agg')
from casper import Casper
from RPROP import RPROP, regularized_loss
from sklearn.metrics import f1_score

import os
import argparse
######################################################################
# Options
# --------
parser = argparse.ArgumentParser(description='Training, evaluating, testing')
parser.add_argument('log_repo_name', help="the repo used to place logs(text & image)")
parser.add_argument("--delta", default=100, type=int, help="period to evaluate add_neuron criterion")
parser.add_argument("--lr_init", default=0.2, type=float, help="initial learning rate in first training phase")
parser.add_argument("--lr_region", default="0.25, 0.005, 0.001", type=str, help="initial learning rates of three different regions, format: l1,l2,l3")
parser.add_argument("--T", default=0.05, type=float, help="temperature constant in simulated annealing")
opt = parser.parse_args()

delta = opt.delta
lr_init = opt.lr_init
lr_region_lst = opt.lr_region.split(",")
lr_region = float(lr_region_lst[0]), float(lr_region_lst[1]), float(lr_region_lst[2])
T = opt.T
# print(lr_region)
# exit()
######################################################################

dataset_repo = "./dataset/"
log_parent_path = "./logs/"
log_repo_name = log_parent_path + opt.log_repo_name # repo to hold text and images
os.mkdir(log_repo_name)
f = open(log_repo_name+"/log.txt", "w") # hold text summarative log
# record loss, accuracy, f1_score of last epoch
final_train = [[],[]]
final_eva = [[],[]]
final_train_f1 = []
# record number of epochs and hidden neuron
all_epochs = []
all_hiddens = []

# carry out 10-fold cross validation
for fold_i in range(10):
    print(f"Fold {fold_i} begin...")
    dataset_fold = dataset_repo+f"fold{fold_i}/"
    # read in train, evaluate, test dataset
    dataset_train = pd.read_excel(dataset_fold+"train_set.xlsx")
    X_train = dataset_train.iloc[:,:10]
    Y_train = dataset_train["label"]
    dataset_eva = pd.read_excel(dataset_fold+"eva_set.xlsx")
    X_eva = dataset_eva.iloc[:,:10]
    Y_eva = dataset_eva["label"]
    dataset_test = pd.read_excel(dataset_fold+"test_set.xlsx")
    X_test = dataset_test.iloc[:,:10]
    Y_test = dataset_test["label"]
    # dataset attributes
    N, D = X_train.shape
    features = X_train.columns

    # define net & its parameters
    net = Casper(input_dim=D, delta=delta, lr_initial=lr_init)
    loss_func = torch.nn.CrossEntropyLoss()
    optimiser = RPROP(lr_region=lr_region)
    optimiser_normal = torch.optim.Adam(net.input_output.parameters(), lr=lr_init) # only used for first phase

    # keep record of training, evaluating, testing process
    epoch = 0
    progress_print = 200
    all_losses_train = []
    all_losses_eva = []
    epoches_add_neuron = []
    all_accuracy_train = []
    all_accuracy_eva = []

    while True:
        """Training phase"""
        # convert to tensor
        X = torch.Tensor(X_train.values).float()
        Y = torch.Tensor(Y_train.values).long()

        Y_pred = net(X)
        loss_raw = loss_func(Y_pred, Y)
        # regularized loss
        loss = regularized_loss(loss_raw, net.weights_lr.keys(), optimiser.Hepoch, T=T)
        all_losses_train.append(loss.item())

        # print progress every
        if epoch % progress_print == 0:
            _, predicted = torch.max(Y_pred, 1)
            # calculate and print accuracy
            total = predicted.size(0)
            correct = predicted.data.numpy() == Y.data.numpy()
            acc = sum(correct)/total
            all_accuracy_train.append(acc)
            train_f1 = f1_score(Y.data.numpy(), predicted.data.numpy())
            # print('Epoch [%d] training Loss: %.4f  Accuracy: %.2f %%'
            #       % (epoch, loss.item(), 100 * acc))

        # same fuctionality as net.zerp_grad()
        for p in net.weights_lr.keys(): # mannualy set all gradient to 0
            if p.grad is not None:
                p.grad = torch.zeros_like(p.grad)
        loss.backward()

        if not epoches_add_neuron: # first phase
            optimiser_normal.step()
        else:
            optimiser.step(net)
        """Training phase end"""


        """Evaluation phase: no gradient"""
        with torch.no_grad():
            X = torch.Tensor(X_eva.values).float()
            Y = torch.Tensor(Y_eva.values).long()

            Y_pred = net(X)
            loss_raw = loss_func(Y_pred, Y)
            loss = regularized_loss(loss_raw, net.weights_lr.keys(), optimiser.Hepoch, T=T)
            all_losses_eva.append(loss.item())
            # print progress
            if epoch % progress_print == 0:
                _, predicted = torch.max(Y_pred, 1)
                # calculate and print accuracy
                total = predicted.size(0)
                correct = predicted.data.numpy() == Y.data.numpy()
                acc = sum(correct)/total
                all_accuracy_eva.append(acc)
                # print('Epoch [%d] evaluation Loss: %.4f  Accuracy: %.2f %%'
                #       % (epoch, loss.item(), 100 * acc))
            epoch +=1

            if net.criterion_add_neuron(eva_losses=all_losses_eva, epoches_add_neuron=epoches_add_neuron):
                # no improvement reported
                if net.criterion_halt(eva_losses=all_losses_eva, epoches_add_neuron=epoches_add_neuron, training_accs=all_accuracy_train):
                    # meet halt criterion
                    final_train_f1.append(train_f1)
                    print("Training ended at epoch:{}".format(epoch))
                    print("Number of hidden neuron:", len(epoches_add_neuron))
                    break
                else:
                    # need add a new hidden
                    epoches_add_neuron.append(epoch)
                    net.add_neuron()
                    optimiser.update_lrs(net)
        """Evaluation phase: no gradient"""

    """Visualization of train & eva, test phase"""
    all_losses_train = np.array(all_losses_train)
    all_losses_eva = np.array(all_losses_eva)
    # filter too large training loss casued by just adding a new hidden neuron
    # for Visualization purpose
    all_losses_train_filter = np.where(all_losses_train<1, all_losses_train, np.nan)
    # visualise all losses
    plt.figure(figsize=(12,9))
    plt.plot(all_losses_train_filter, label="train loss")
    plt.plot(all_losses_eva, label="eva loss")
    plt.legend()
    plt.savefig(log_repo_name+f"/loss-{fold_i}", bbox_inches="tight")

    """Testing phase"""
    X = torch.Tensor(X_test.values).float()
    Y = torch.Tensor(Y_test.values).long()

    Y_pred = net(X)
    _, predicted = torch.max(Y_pred, 1)
    # calculate accuracy
    total_test = predicted.size(0)
    correct_test = sum(predicted.data.numpy() == Y.data.numpy())
    print('Testing Accuracy: %.2f %%' % (100 * correct_test / total_test))

    confusion_test = torch.zeros(2, 2)
    for i in range(Y_test.shape[0]):
        actual_class = Y.data[i]
        predicted_class = predicted.data[i]
        confusion_test[actual_class][predicted_class] += 1
    # print('')
    # print('Confusion matrix for testing:')
    # print(confusion_test)

    print(f"Fold {fold_i} end...")

    # write summary to log
    final_train[0].append(all_losses_train[-1])
    final_train[1].append(all_accuracy_train[-1])
    final_eva[0].append(all_losses_eva[-1])
    final_eva[1].append(all_accuracy_eva[-1])
    all_epochs.append(len(all_losses_train))
    all_hiddens.append(len(epoches_add_neuron))
    f.write(f"{fold_i} summary:\n")
    f.write(f"# Epoches:{len(all_losses_train)}, # hidden neuros:{len(epoches_add_neuron)}\n")
    f.write("Final loss and accuracy:\n")
    f.write("Training   - Loss: %.4f  Accuracy: %.2f %%\n" %(all_losses_train[-1],100*all_accuracy_train[-1]))
    f.write("Evaluating - Loss: %.4f  Accuracy: %.2f %%\n" %(all_losses_eva[-1],100*all_accuracy_eva[-1]))
    f.write('Testing    -             Accuracy: %.2f %%\n\n' %(100 * correct_test / total_test))

f.write(f'\nTraing loss: mean {sum(final_train[0])/len(final_train[0])}, max {max(final_train[0])}')
f.write(f'\nTraing acc: mean {sum(final_train[1])/len(final_train[1])}, max {max(final_train[1])}')
f.write("\n"+str(final_train[1]))
# f.write(f'\nEvaluating loss: mean {sum(final_eva[0])/len(final_eva[0])}, max {max(final_eva[0])}')
# f.write(f'\nEvaluating acc: mean {sum(final_eva[1])/len(final_eva[1])}, max {max(final_eva[1])}')
f.write(f'\nTraing f1: mean {sum(final_train_f1)/len(final_train_f1)}, max {max(final_train_f1)}')
f.write("\n"+str(final_train_f1))
f.write(f'\nTraing epochs: mean {sum(all_epochs)/len(all_epochs)}, max {max(all_epochs)}')
f.write("\n"+str(all_epochs))
f.write(f'\nTraing hiddens: mean {sum(all_hiddens)/len(all_hiddens)}, max {max(all_hiddens)}')
f.write("\n"+str(all_hiddens))
f.close()

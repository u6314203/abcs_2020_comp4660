# README

Report: [Genetic Algorithm for Hyperparameter Tuning Maximizing Convergence Speed and Ability](./u6314203.pdf)

## Dataset

All datasets are placed in the folder *dataset*:

+ thermal-RGB-components: original dataset
+ preproc_base_thermal: recode and discard certain columns
+ preproc_norm_thermal: normalize all feature columns

## Training & evaluating

Due to the limited time, a command line interface is not provided, all relevant codes are in the file:

*node/convergence_GA.ipynb*

For testing different GA architectures and parameters, please change accordingly in the last section of notebook:

```python
POP_SIZE = 20            # population size
CROSS_RATE = 0.8          # DNA crossover probability
MUTATION_RATE = 0.01     # mutation probability
N_GENERATIONS = 20       # generation size

log_parent_path = "../logs/"
log_name = f"pop{POP_SIZE}_gen{N_GENERATIONS}_crossp{CROSS_RATE}_mutatep{MUTATION_RATE}_sbx_propotionalSelect.txt"
f = open(log_parent_path+log_name, "w")
```

```python
#     for i, parent in enumerate(population):
#         # produce a child by crossover operation
#         child_gene_dict = crossover(parent, pop_copy)
#         # clamp into range
#         clamp(child_gene_dict)
#         # mutate child
#         mutate(child_gene_dict)
#         # replace parent with its child
#         child = Chromosome()
#         child.set_genotype(child_gene_dict)
#         population[i] = child
    for i, parent in enumerate(population):
        if i >= 0.5*len(population):
            break
        # produce two children by crossover operation
        child1_gene_dict, child2_gene_dict = crossover(parent, pop_copy, method=2)
        # clamp into range
        clamp(child1_gene_dict)
        clamp(child2_gene_dict)
        # mutate child
        mutate(child1_gene_dict)
        mutate(child2_gene_dict)
        # replace parent with its child
        child1 = Chromosome()
        child2 = Chromosome()
        child1.set_genotype(child1_gene_dict)
        child2.set_genotype(child2_gene_dict)
        population[2*i] = child1
        population[2*i+1] = child2
```

For use of blend crossover rather than simulated binary crossover,  do a comment switch for above code section.

The result shall be written into folder *logs* by specified *log_name*. 

## Contact

For any issue, please contact email: xinqi.zhu@anu.edu.au
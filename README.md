## README

For paper submission to this conference (also as part of course assessment, to maximise convergency ability and speed of an ANN), two techniques are examined – replacing fully connected ANN with Cascade architecture in submission1 and searching for best hyperparameter setting by GA in submission2.For paper submission to this conference (also as part of course assessment, to maximise convergency ability and speed of an ANN), two techniques are examined – replacing fully connected ANN with Cascade architecture in [submission1](./submission1/) and searching for best hyperparameter setting by GA in [submission2](./submission2).

## Contact

For any issue, please contact: xinqi.zhu@anu.edu.au

